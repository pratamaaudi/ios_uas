
import Foundation

protocol ConnectionProtocol: class {
    func getResult(result: NSArray, query: String, error: String)
}

class ConnectionModel: NSObject, URLSessionDataDelegate {
    weak var delegate: ConnectionProtocol!
    let urlPath = "http://127.0.0.1:8888/ios/controller.php"
    
    var param: String = ""
    var urlRequest: String = ""
    
    var key = "adsfa"
    
    
    func requestServicePost(query: String, param: String)
    {
        var request = URLRequest(url: URL(string: urlPath)!)
        request.httpMethod = "POST"
        request.httpBody = param.data(using: .utf8)
        let defaultSession = Foundation.URLSession(configuration: .default)
        let task = defaultSession.dataTask(with: request) { (data, response, error) in
            if error != nil {
                print("Failed to download data: \((error?.localizedDescription) ?? "error")")
                DispatchQueue.main.async {
                    let result_data = NSMutableArray()
                    self.delegate.getResult(result: result_data, query: query, error: "Network error. Cannot connect to server.")
                }
            } else {
                print("Data downloaded - \(query)")
//                print("hai   \(data!)")
                
                self.parseJSON(data!, query)
            }
        }
        task.resume()
    }
    
    
    func parseJSON(_ data: Data, _ query: String) {
        var jsonResult = NSArray()
        
        print(data)
        do {
            jsonResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSArray
        } catch let error as NSError {
            print(error)
        }
        
        
        var jsonElement = NSDictionary()
        var result_data = NSMutableArray()
        
        for i in 0 ..< jsonResult.count
        {
            jsonElement = jsonResult[i] as! NSDictionary
            
            switch query {
            case "REGISTER":
                if let error = jsonElement["error"]{
                    result_data.add("error")
                    result_data.add(error)
                }
                else if let token = jsonElement["token"]{
                    result_data.add("success")
                    result_data.add(token);
                }
            case "login":
                print(jsonElement["res"])
//                if let error = jsonElement["error"]{
//                    result_data.add("error")
//                    result_data.add(error)
//                }
//                else if let data = jsonElement["token"]{
//                    result_data.add("success")
//                    result_data.add(token);
//                }
            default:
                result_data = NSMutableArray()
            }
            
        }
        
        DispatchQueue.main.async {
            self.delegate.getResult(result: result_data, query: query, error: "")
        }
    }
    
    
    
    
    
    func sendQuery(query: String) {
        print("query received")
        urlRequest = urlPath + "jenis=" + query + "&token=" + key
        
        requestService(query: query)
    }
    
    func sendQuery(query: String, param1: String) {
        urlRequest = urlPath + query + "/" + key + "/" + param1
        requestService(query: query)
    }
    
    func sendQueryPost(query: String, param: String) {
        urlRequest = urlPath + query
        requestServicePost(query: query, param: param)
    }
    
    
    func requestService(query: String)
    {
        print("request")
        let url: URL = URL(string: urlRequest)!
        let defaultSession = Foundation.URLSession(configuration: .default)
        let task = defaultSession.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                print("Failed to download data: \((error?.localizedDescription) ?? "error")")
                
                DispatchQueue.main.async {
                    let result_data = NSMutableArray()
                    self.delegate.getResult(result: result_data, query: query, error: "Network error. Cannot connect to server.")
                }
            } else {
                print("Data downloaded - \(query)")
                self.parseJSON(data!, query)
            }
        }
        task.resume()
    }
    
    
     
    
    
}


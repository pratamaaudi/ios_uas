//
//  LoginViewController.swift
//  projectUas
//
//  Created by informatika02 on 12/11/17.
//  Copyright © 2017 audi. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, ConnectionProtocol {

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var btnMasuk: UIButton!
    
    
    func getResult(result: NSArray, query: String, error: String) {
        if query == "LOGIN"
        {
            if let err = result.object(at: 1) as? String{
                let alert = UIAlertController(title: "Alert", message: err, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func btnMasukOnClick(_ sender: Any) {
        if let username = username.text, let pass = password.text{
            if username != "" && pass != "" {
                let conn = ConnectionModel();
                conn.delegate = self;
                let param:String = "username="+username+"&password="+pass+"&act=LOGIN"
                conn.requestServicePost(query: "login", param: param)
            }
            else if (username == ""){
                funcAlert("Isi Username !!")
            }
            else if (pass == ""){
                funcAlert("Isi Password")
            }
        }
    }
    
    func  funcAlert(_ mess:String) {
        // create the alert
        let alert = UIAlertController(title: mess, message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        // add the actions (buttons)
//        alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil))
        
        // show the alert
        return self.present(alert, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

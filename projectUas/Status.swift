//
//  Status.swift
//  TableView
//
//  Created by informatika on 11/6/17.
//  Copyright © 2017 ubaya. All rights reserved.
//

import Foundation

class Status {
    public var user:User?
    public var status:String?
    public var date:String?
    
     init() {
        user = User()
        status = ""
        date = ""
    }
    
    
    enum Jenis:String {
        case showallstatus = "showallstatus"
        case showoneuser = "showoneuser"
    }    
}

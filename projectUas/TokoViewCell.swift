//
//  TokoViewCell.swift
//  projectUas
//
//  Created by informatika02 on 12/11/17.
//  Copyright © 2017 audi. All rights reserved.
//

import UIKit

class TokoViewCell: UICollectionViewCell {
    
    @IBOutlet weak var gambarToko: UIImageView!
    @IBOutlet weak var namaToko: UILabel!
    
}

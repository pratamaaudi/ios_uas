//
//  User.swift
//  TableView
//
//  Created by informatika on 11/6/17.
//  Copyright © 2017 ubaya. All rights reserved.
//

import Foundation

class User {
    public var username:String?
    public var email:String?
    public var id:Int?
    
     init() {
        username = ""
        email = ""
        id = 0
    }
    
    
    public func User(username:String, email:String){
        self.username = username
        self.email = email
    }
    
}
